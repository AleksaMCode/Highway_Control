﻿namespace HC_entry
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.txtPlate = new System.Windows.Forms.TextBox();
            this.labelRegPlate = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Panel();
            this.labelCreate = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendFeedbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportAProblemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.provideASuggestionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutHighwayControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.labelSecEntry = new System.Windows.Forms.Label();
            this.btnMinimize = new System.Windows.Forms.PictureBox();
            this.groupBoxAbout = new System.Windows.Forms.GroupBox();
            this.richTextBoxAbout = new System.Windows.Forms.RichTextBox();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.btnCloseGroup = new System.Windows.Forms.PictureBox();
            this.btnCreate.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimize)).BeginInit();
            this.groupBoxAbout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCloseGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // txtPlate
            // 
            this.txtPlate.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.txtPlate.Location = new System.Drawing.Point(85, 77);
            this.txtPlate.Margin = new System.Windows.Forms.Padding(2);
            this.txtPlate.Name = "txtPlate";
            this.txtPlate.Size = new System.Drawing.Size(132, 28);
            this.txtPlate.TabIndex = 0;
            this.txtPlate.TextChanged += new System.EventHandler(this.txtPlate_TextChanged);
            this.txtPlate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPlate_KeyDown);
            // 
            // labelRegPlate
            // 
            this.labelRegPlate.AutoSize = true;
            this.labelRegPlate.BackColor = System.Drawing.Color.White;
            this.labelRegPlate.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelRegPlate.Font = new System.Drawing.Font("Microsoft Tai Le", 11F);
            this.labelRegPlate.ForeColor = System.Drawing.Color.Silver;
            this.labelRegPlate.Location = new System.Drawing.Point(88, 81);
            this.labelRegPlate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelRegPlate.Name = "labelRegPlate";
            this.labelRegPlate.Size = new System.Drawing.Size(127, 19);
            this.labelRegPlate.TabIndex = 1;
            this.labelRegPlate.Text = "Registration plate";
            this.labelRegPlate.Click += new System.EventHandler(this.labelRegPlate_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(154)))), ((int)(((byte)(167)))));
            this.btnCreate.Controls.Add(this.labelCreate);
            this.btnCreate.Location = new System.Drawing.Point(106, 119);
            this.btnCreate.Margin = new System.Windows.Forms.Padding(2);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(88, 32);
            this.btnCreate.TabIndex = 0;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            this.btnCreate.MouseEnter += new System.EventHandler(this.btnCreate_MouseEnter);
            this.btnCreate.MouseLeave += new System.EventHandler(this.btnCreate_MouseLeave);
            // 
            // labelCreate
            // 
            this.labelCreate.AutoSize = true;
            this.labelCreate.Font = new System.Drawing.Font("Microsoft Tai Le", 13F, System.Drawing.FontStyle.Bold);
            this.labelCreate.ForeColor = System.Drawing.Color.White;
            this.labelCreate.Location = new System.Drawing.Point(15, 5);
            this.labelCreate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelCreate.Name = "labelCreate";
            this.labelCreate.Size = new System.Drawing.Size(65, 23);
            this.labelCreate.TabIndex = 0;
            this.labelCreate.Text = "Create";
            this.labelCreate.Click += new System.EventHandler(this.labelCreate_Click);
            this.labelCreate.MouseEnter += new System.EventHandler(this.labelCreate_MouseEnter);
            this.labelCreate.MouseLeave += new System.EventHandler(this.labelCreate_MouseLeave);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(154)))), ((int)(((byte)(167)))));
            this.menuStrip1.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(300, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createToolStripMenuItem,
            this.toolStripSeparator1,
            this.logoutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // createToolStripMenuItem
            // 
            this.createToolStripMenuItem.Name = "createToolStripMenuItem";
            this.createToolStripMenuItem.Size = new System.Drawing.Size(118, 26);
            this.createToolStripMenuItem.Text = "Create";
            this.createToolStripMenuItem.Click += new System.EventHandler(this.createToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(115, 6);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(118, 26);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::HC_entry.Properties.Resources.Cancel_96;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(118, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewHelpToolStripMenuItem,
            this.sendFeedbackToolStripMenuItem,
            this.toolStripSeparator2,
            this.aboutHighwayControlToolStripMenuItem});
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // viewHelpToolStripMenuItem
            // 
            this.viewHelpToolStripMenuItem.Image = global::HC_entry.Properties.Resources.Question_Mark_96;
            this.viewHelpToolStripMenuItem.Name = "viewHelpToolStripMenuItem";
            this.viewHelpToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F11)));
            this.viewHelpToolStripMenuItem.Size = new System.Drawing.Size(249, 26);
            this.viewHelpToolStripMenuItem.Text = "View Help";
            this.viewHelpToolStripMenuItem.ToolTipText = "Coming Soon";
            // 
            // sendFeedbackToolStripMenuItem
            // 
            this.sendFeedbackToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportAProblemToolStripMenuItem,
            this.provideASuggestionToolStripMenuItem});
            this.sendFeedbackToolStripMenuItem.Name = "sendFeedbackToolStripMenuItem";
            this.sendFeedbackToolStripMenuItem.Size = new System.Drawing.Size(249, 26);
            this.sendFeedbackToolStripMenuItem.Text = "Send Feedback";
            this.sendFeedbackToolStripMenuItem.ToolTipText = "Coming Soon";
            // 
            // reportAProblemToolStripMenuItem
            // 
            this.reportAProblemToolStripMenuItem.Name = "reportAProblemToolStripMenuItem";
            this.reportAProblemToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.reportAProblemToolStripMenuItem.Text = "Report a Problem...";
            this.reportAProblemToolStripMenuItem.ToolTipText = "Coming Soon";
            // 
            // provideASuggestionToolStripMenuItem
            // 
            this.provideASuggestionToolStripMenuItem.Name = "provideASuggestionToolStripMenuItem";
            this.provideASuggestionToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.provideASuggestionToolStripMenuItem.Text = "Provide a Suggestion...";
            this.provideASuggestionToolStripMenuItem.ToolTipText = "Coming Soon";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(246, 6);
            // 
            // aboutHighwayControlToolStripMenuItem
            // 
            this.aboutHighwayControlToolStripMenuItem.Name = "aboutHighwayControlToolStripMenuItem";
            this.aboutHighwayControlToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.aboutHighwayControlToolStripMenuItem.Size = new System.Drawing.Size(249, 26);
            this.aboutHighwayControlToolStripMenuItem.Text = "About Highway Control";
            this.aboutHighwayControlToolStripMenuItem.Click += new System.EventHandler(this.aboutHighwayControlToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(181, 26);
            this.toolStripMenuItem1.Text = "Crate";
            // 
            // labelSecEntry
            // 
            this.labelSecEntry.AutoSize = true;
            this.labelSecEntry.BackColor = System.Drawing.Color.Transparent;
            this.labelSecEntry.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.labelSecEntry.ForeColor = System.Drawing.Color.Black;
            this.labelSecEntry.Location = new System.Drawing.Point(99, 167);
            this.labelSecEntry.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSecEntry.Name = "labelSecEntry";
            this.labelSecEntry.Size = new System.Drawing.Size(105, 18);
            this.labelSecEntry.TabIndex = 4;
            this.labelSecEntry.Text = "Successful entry";
            this.labelSecEntry.Visible = false;
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(154)))), ((int)(((byte)(167)))));
            this.btnMinimize.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimize.Image")));
            this.btnMinimize.Location = new System.Drawing.Point(277, -2);
            this.btnMinimize.Margin = new System.Windows.Forms.Padding(2);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(22, 24);
            this.btnMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnMinimize.TabIndex = 5;
            this.btnMinimize.TabStop = false;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            this.btnMinimize.MouseEnter += new System.EventHandler(this.btnMinimize_MouseEnter);
            this.btnMinimize.MouseLeave += new System.EventHandler(this.btnMinimize_MouseLeave);
            this.btnMinimize.MouseHover += new System.EventHandler(this.btnMinimize_MouseHover);
            // 
            // groupBoxAbout
            // 
            this.groupBoxAbout.Controls.Add(this.richTextBoxAbout);
            this.groupBoxAbout.Controls.Add(this.pictureBoxLogo);
            this.groupBoxAbout.Controls.Add(this.btnCloseGroup);
            this.groupBoxAbout.Font = new System.Drawing.Font("Microsoft Tai Le", 7.8F);
            this.groupBoxAbout.Location = new System.Drawing.Point(1, 24);
            this.groupBoxAbout.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxAbout.Name = "groupBoxAbout";
            this.groupBoxAbout.Padding = new System.Windows.Forms.Padding(2);
            this.groupBoxAbout.Size = new System.Drawing.Size(299, 181);
            this.groupBoxAbout.TabIndex = 6;
            this.groupBoxAbout.TabStop = false;
            this.groupBoxAbout.Text = "About Highway Control";
            this.groupBoxAbout.Visible = false;
            // 
            // richTextBoxAbout
            // 
            this.richTextBoxAbout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(154)))), ((int)(((byte)(167)))));
            this.richTextBoxAbout.Font = new System.Drawing.Font("Microsoft Tai Le", 8.5F);
            this.richTextBoxAbout.ForeColor = System.Drawing.Color.White;
            this.richTextBoxAbout.Location = new System.Drawing.Point(71, 51);
            this.richTextBoxAbout.Margin = new System.Windows.Forms.Padding(2);
            this.richTextBoxAbout.Name = "richTextBoxAbout";
            this.richTextBoxAbout.ReadOnly = true;
            this.richTextBoxAbout.Size = new System.Drawing.Size(224, 80);
            this.richTextBoxAbout.TabIndex = 0;
            this.richTextBoxAbout.Text = resources.GetString("richTextBoxAbout.Text");
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Image = global::HC_entry.Properties.Resources.logo_road_blue1;
            this.pictureBoxLogo.Location = new System.Drawing.Point(-13, 49);
            this.pictureBoxLogo.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(95, 88);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TabIndex = 7;
            this.pictureBoxLogo.TabStop = false;
            // 
            // btnCloseGroup
            // 
            this.btnCloseGroup.Image = global::HC_entry.Properties.Resources.Cancel_96;
            this.btnCloseGroup.Location = new System.Drawing.Point(284, 7);
            this.btnCloseGroup.Margin = new System.Windows.Forms.Padding(2);
            this.btnCloseGroup.Name = "btnCloseGroup";
            this.btnCloseGroup.Size = new System.Drawing.Size(14, 15);
            this.btnCloseGroup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnCloseGroup.TabIndex = 7;
            this.btnCloseGroup.TabStop = false;
            this.btnCloseGroup.Click += new System.EventHandler(this.btnCloseGroup_Click);
            this.btnCloseGroup.MouseEnter += new System.EventHandler(this.btnCloseGroup_MouseEnter);
            this.btnCloseGroup.MouseLeave += new System.EventHandler(this.btnCloseGroup_MouseLeave);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(300, 206);
            this.Controls.Add(this.groupBoxAbout);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.labelSecEntry);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.labelRegPlate);
            this.Controls.Add(this.txtPlate);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.btnCreate.ResumeLayout(false);
            this.btnCreate.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimize)).EndInit();
            this.groupBoxAbout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCloseGroup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPlate;
        private System.Windows.Forms.Label labelRegPlate;
        private System.Windows.Forms.Panel btnCreate;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutHighwayControlToolStripMenuItem;
        private System.Windows.Forms.Label labelCreate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Label labelSecEntry;
        private System.Windows.Forms.PictureBox btnMinimize;
        private System.Windows.Forms.ToolStripMenuItem sendFeedbackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportAProblemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem provideASuggestionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.GroupBox groupBoxAbout;
        private System.Windows.Forms.PictureBox btnCloseGroup;
        private System.Windows.Forms.RichTextBox richTextBoxAbout;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
    }
}