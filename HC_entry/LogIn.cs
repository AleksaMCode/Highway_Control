﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; // use System.IO namespace for reading and writing to files and data streams

namespace HC_entry
{
    public partial class LogIn : Form
    {
        string[] array;
        public LogIn()
        {
            InitializeComponent();
        }

        private void setFormElements()
        {
            labelUsername.Visible = labelPassword.Visible = true;
            txtUsername.Text = txtPassword.Text = "";
            txtUsername.Focus();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text == "" || txtPassword.Text == "") // handle error input
            {
                MessageBox.Show("Invalid Login. Please enter correct username or password.", "Warning",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                setFormElements();          
            }
            else // read line from a text file and compare user entered username and password with ones already in a database (text file)
            {
                string line;
                StreamReader file = new StreamReader("Users_entry.txt");

                while ((line = file.ReadLine()) != null)
                {
                    array = line.Split(' ');
                    if (string.Equals(array[0], txtUsername.Text)
                        && string.Equals(array[1], txtPassword.Text))
                        break;
                }

                file.Close();

                if (line == null) // handle wrong username or password input (no match is found)
                {
                    MessageBox.Show("Wrong username or password.");
                    setFormElements();
                }
                else
                {
                    // close this wform and open MainForm
                    this.Close();
                    MainForm mainForm = new MainForm();
                    mainForm.getdata = array[2]; // transfer node (entry location) to form MainForm
                    mainForm.Show();
                }
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            labelPassword.Visible = false;

            if (txtPassword.Text == "" && (e.KeyData == Keys.Back || e.KeyData == Keys.Delete))
                labelPassword.Visible = true;

            if (e.KeyData == Keys.Enter)
                btnLogin_Click(sender, e);
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            labelUsername.Visible = false;

            if (txtUsername.Text == "" && (e.KeyData == Keys.Back || e.KeyData == Keys.Delete))
                labelUsername.Visible = true;

            if (e.KeyData == Keys.Enter)
                btnLogin_Click(sender, e);
        }

        // btnClose_MouseEnter && btnClose_MouseLeave simulate a button like behaviour on a picture box "btnClose"
        private void btnClose_MouseEnter(object sender, EventArgs e) => this.btnClose.BackColor = Color.FromArgb(14, 184, 167);

        private void btnClose_MouseLeave(object sender, EventArgs e) => this.btnClose.BackColor = Color.Transparent;

        // btnLogin_MouseEnter && btnLogin_MouseLeave && label1_MouseEnter && label1_MouseLeave
        // simulate a button like behaviour on a picture box "btnLogin"
        private void btnLogin_MouseEnter(object sender, EventArgs e) => this.btnLogin.BackColor = Color.FromArgb(14, 184, 167);

        private void btnLogin_MouseLeave(object sender, EventArgs e) => this.btnLogin.BackColor = Color.FromArgb(14, 154, 167);

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {
            if (txtUsername.Text == "") labelUsername.Visible = true;
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            if (txtPassword.Text == "") labelPassword.Visible = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Do you really want to close the program?", "Warning", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
                Application.ExitThread();
        }

        private void label1_MouseEnter(object sender, EventArgs e) => btnLogin_MouseEnter(sender, e);

        private void label1_MouseLeave(object sender, EventArgs e) => btnLogin_MouseLeave(sender, e);

        private void label1_Click(object sender, EventArgs e) => btnLogin_Click(sender, e);

        private void labelUsername_Click(object sender, EventArgs e) => txtUsername.Focus();

        private void labelPassword_Click(object sender, EventArgs e) => txtPassword.Focus();

        // show a tooltip when you hover over btnClose with a mouse
        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.btnClose, "Exit");
        }
    }
}