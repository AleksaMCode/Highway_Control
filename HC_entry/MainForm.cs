﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; // use System.IO namespace for reading and writing to files and data streams

namespace HC_entry
{
    public partial class MainForm : Form
    {
        public string getdata; // entry node
        bool etc = false; // electronic toll collection
        public MainForm()
        {
            InitializeComponent();
        }

        // decide if driver has ETC
        private void etc_Check()
        {
            DialogResult dialog = MessageBox.Show("Does driver have an ETC?", "Question", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
                etc = true;
            else
                etc = false;
        }

        // duplicate plates in the database (text file) are forbidden
        private bool duplicate_Plate_Check(string regPlate)
        {
            if (File.Exists("Drivers.txt"))
            {
                string line;
                StreamReader file = new StreamReader("Drivers.txt");

                while ((line = file.ReadLine()) != null)
                {
                    string[] sline = line.Split(' ');

                    if (string.Equals(sline[0], regPlate))
                    {
                        file.Close();
                        return true;
                    }
                }

                file.Close();
            }
            return false;
        }

        private void setFormElements()
        {
            txtPlate.Text = "";
            labelRegPlate.Visible = true;
            txtPlate.Focus();
        }

        private void warning_Error()
        {
            MessageBox.Show("Please enter valid registration plate.", "Warning",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            setFormElements();
        }

        // write in a text file -> registration plate, location of entry, and time and date
        private void write_Driver(StreamWriter writer)
        {
            string line;

            if (etc)
                line = String.Format("{0} {1} {2} {3}", txtPlate.Text, getdata, DateTime.Now.ToString(), "1".ToString());
            else
                line = String.Format("{0} {1} {2} {3}", txtPlate.Text, getdata, DateTime.Now.ToString(), "0".ToString());

            writer.WriteLine(line);
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (txtPlate.Text == "" || txtPlate.Text.Length > 10) // handle error input
                warning_Error();
            else
            {
                if (duplicate_Plate_Check(txtPlate.Text) == false)
                {
                    etc_Check();
                    StreamWriter writer;

                    if (!File.Exists("Drivers.txt")) // if file doesn't exist, create a new one
                    {
                        using (writer = new StreamWriter("Drivers.txt"))
                        {
                            write_Driver(writer);
                        }
                    }
                    else // if file does exist, append in a existing one
                    {
                        using (writer = File.AppendText("Drivers.txt"))
                        {
                            write_Driver(writer);
                        }
                    }

                    setFormElements();
                    // create a timer and hide labelSecEntry after 1.5 sec. using that timer
                    var t = new Timer();
                    labelSecEntry.Visible = true;
                    t.Interval = 1500;
                    t.Tick += (s, e2) =>
                    {
                        labelSecEntry.Visible = false;
                        t.Stop();
                    };
                    t.Start();
                }
                else
                    warning_Error();
            }
        }

        // logout event
        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            LogIn li = new LogIn();
            li.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Do you really want to close the program?", "Warning", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
                Application.ExitThread();
        }

        private void txtPlate_KeyDown(object sender, KeyEventArgs e)
        {
            labelRegPlate.Visible = false;

            if (txtPlate.Text == "" && (e.KeyData == Keys.Back || e.KeyData == Keys.Delete))
                labelRegPlate.Visible = true;

            if (e.KeyData == Keys.Enter)
                btnCreate_Click(sender, e);
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e) => btnCreate_Click(sender, e);

        private void txtPlate_TextChanged(object sender, EventArgs e)
        {
            if (txtPlate.Text == "") labelRegPlate.Visible = true;
        }

        // btnMinimize_MouseEnter && btnMinimize_MouseLeave simulate a button like behaviour on a picture box "btnMinimize"
        private void btnMinimize_MouseEnter(object sender, EventArgs e) => this.btnMinimize.BackColor = Color.FromArgb(14, 184, 167);

        private void btnMinimize_MouseLeave(object sender, EventArgs e) => this.btnMinimize.BackColor = Color.FromArgb(14, 154, 167);

        private void btnMinimize_Click(object sender, EventArgs e) => this.WindowState = FormWindowState.Minimized;

        private void labelCreate_Click(object sender, EventArgs e) => btnCreate_Click(sender, e);

        // btnCreate_MouseEnter && btnCreate_MouseLeave && labelCreate_MouseEnter && labelCreate_MouseLeave         
        // simulate a button like behaviour on a picture box "btnCreate"
        private void btnCreate_MouseEnter(object sender, EventArgs e) => this.btnCreate.BackColor = Color.FromArgb(14, 184, 167);

        private void btnCreate_MouseLeave(object sender, EventArgs e) => this.btnCreate.BackColor = Color.FromArgb(14, 154, 167);

        private void labelCreate_MouseEnter(object sender, EventArgs e) => btnCreate_MouseEnter(sender, e);

        private void labelCreate_MouseLeave(object sender, EventArgs e) => btnCreate_MouseLeave(sender, e);

        private void labelRegPlate_Click(object sender, EventArgs e) => txtPlate.Focus();

        // show a tooltip when you hover over btnMinimize with a mouse
        private void btnMinimize_MouseHover(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.btnMinimize, "Minimize");
        }

        // close About tab
        private void btnCloseGroup_Click(object sender, EventArgs e)
        {
            groupBoxAbout.Visible = false;

            if (labelRegPlate.Visible == false)
                labelRegPlate.Visible = true;
        }

        // open About tab
        private void aboutHighwayControlToolStripMenuItem_Click(object sender, EventArgs e) => groupBoxAbout.Visible = true;

        // simulate a button like behaviour on a picture box "btnCloseGroup"
        private void btnCloseGroup_MouseEnter(object sender, EventArgs e) => this.btnCloseGroup.BackColor = Color.Gainsboro;

        private void btnCloseGroup_MouseLeave(object sender, EventArgs e) => this.btnCloseGroup.BackColor = Color.Silver;
    }
}
