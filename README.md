Welcome to Highway Control! 
-------------------

Highway Control is a modern open-source program created for an assignment in Basic Principles of Software Engineering course, that's *built* in C#.

Highway Control may have reached version 1, but we're not stopping there. We have many feature ideas that we're anxious to add to Highway Control.

So take Highway Control out for a spin and let us know how we can make it better.
You can see some 
[screenshots of Highway Control](https://gitlab.com/AleksaMCode/Highway_Control/wikis/highway-control-screenshots)
on the wiki.

How to install and run Highway Control
-------------------------------
#### Download

* Installer (setup) for the latest stable build for Windows can be [downloaded here](https://mega.nz/#!KtETmBIC!CVnOHj03JGwMMc3rLLnkXBWoXhZ0BlHxR1haKgRSfEw).

* [Download portable version.](https://mega.nz/#!D0MkmKaJ!MDbrStmAsDsfNnN3zLHarG0y35a7L88nfjr8sX4OEiY)

* [Download project files.](https://gitlab.com/AleksaMCode/Highway_Control/repository/archive.zip?ref=master)

#### Install

[Step by step guide is available in wiki section of the git.](https://gitlab.com/AleksaMCode/Highway_Control/wikis/installation-instructions)

#### Usage

Highway Control should be pretty self-explanatory. [More information is available on wiki page.](https://gitlab.com/AleksaMCode/Highway_Control/wikis/home)


Helping Highway Control
----------------

#### I found a bug!

If you found a repeatable bug send us an email.

Include steps to consistently reproduce the problem, actual vs. expected
results, screenshots, and Highway Control version number. 


#### I have a new suggestion, but don't know how to program!

For feature requests please first check our [Trello board](https://trello.com/b/71WZ6z5t/projektni-zadatak) 
to see if it's already there. If not, feel free to send as an email.


I want to keep track of how Highway Control is doing!
----------------------------------------------

Not sure you needed the exclamation point there, but we like your enthusiasm.

#### Contact info

* **E-mail:** 
 [aleksamcode@gmail.com](mailto:aleksamcode@gmail.com),
[gordana1005@hotmail.com](mailto:gordana1005@hotmail.com),
[andreabaco1996@gmail.com](mailto:andreabaco1996@gmail.com),
[aleksije@outlook.com](mailto:aleksije@outlook.com),
[nessossin@gmail.com](mailto:nessossin@gmail.com)

---