﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; // use System.IO namespace for reading and writing to files and data streams

namespace HC_exit
{
    public partial class MainForm : Form
    {
        public string entryCity;
        public string operatorsName;
        StreamReader fileMat = new StreamReader("Matrix.txt");
        List<List<int>> adjMatrix = new List<List<int>>();
        double[] coefficient = { 0.5, 1.1, 2.2, 3.3 };
        string[] cities;
        const int maxSpeed = 120;
        bool penalty = false;

        public MainForm()
        {
            InitializeComponent();
            string line;

            if ((line = fileMat.ReadLine()) != null)
                cities = line.Split(' ');

            while ((line = fileMat.ReadLine()) != null)
            {
                string[] arrTemp = line.Split(' ');
                int[] myInts = Array.ConvertAll(arrTemp, int.Parse);
                adjMatrix.Add(myInts.OfType<int>().ToList());
            }

            fileMat.Close();
        }

        private void warning_Error()
        {
            MessageBox.Show("Invalid Submit. Please enter valid registration plate number, category or paid amount or check if Drivers.txt file exist.", "Warning",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);

            labelRegPlate.Visible = true;
            labelPaid.Visible = true;
            txtRegPlate.Text = "";
            txtPaid.Text = "";
            dropCategory.Focus();
        }

        private void write_a_ticket(StreamWriter writer, ref string breakLine, ref string line, ref DateTime currentTime, int index, int index2, ref double bill, ref string isItEtc, ref double paid)
        {
            writer.WriteLine(breakLine);

            if (string.Equals(isItEtc, "1")) // handle a case when driver has ETC
                line = String.Format("{0}\r\nOperator: {1}\r\nDate and Time: {2}\r\nRelation: {3} - {4}\r\nPlace: {5}\r\nCategory: {6}\r\nBill: {7}$\r\nMethods of payment: {8}\n",
                    "HIGHWAY CONTROL INC.", operatorsName, currentTime.ToString(), cities[index2], cities[index], cities[index], (dropCategory.SelectedIndex + 1).ToString(), bill.ToString(), "ETC");
            else
                line = String.Format("{0}\r\nOperator: {1}\r\nDate and Time: {2}\r\nRelation: {3} - {4}\r\nPlace: {5}\r\nCategory: {6}\r\nBill: {7}$\r\nPaid: {8}$\r\nThe refund amount: {9}$\r\nMethods of payment: {10}\n",
                    "HIGHWAY CONTROL INC.", operatorsName, currentTime.ToString(), cities[index2], cities[index], cities[index], (dropCategory.SelectedIndex + 1).ToString(), bill.ToString(), txtPaid.Text, (paid - bill).ToString(), "Cash");

            if (penalty)
                line += "\r\nYou drove too fast. You are expected to pay penalty.\n";

            line += "\r\nThank you. Have a safe trip.";
            writer.WriteLine(line);
        }

        private void lineChanger(int line_to_edit)
        {
            string[] arrLine = File.ReadAllLines("Drivers.txt");
            arrLine[line_to_edit] = "";
            File.WriteAllLines("Drivers.txt", arrLine);
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (txtRegPlate.Text == "" || dropCategory.Text == "") // handle error input
                warning_Error();
            else
            {

                if (File.Exists("Drivers.txt"))
                {
                    string line;
                    int lineCount = -1;
                    StreamReader fileDrivers = new StreamReader("Drivers.txt");

                    while ((line = fileDrivers.ReadLine()) != null)
                    {
                        lineCount++;
                        string[] sline = line.Split(' ');

                        if (string.Equals(sline[0], txtRegPlate.Text))
                            break;
                    }

                    if (line == null) // handle if reg. plate is not found in a database (text file)
                    {
                        fileDrivers.Close();
                        warning_Error();
                    }
                    else
                    {
                        string[] driverData = line.Split(' ');

                        if (string.Equals(driverData[5], "0") && txtPaid.Text == "") // handle if driver isn't etc and paid amount is empty
                        {
                            fileDrivers.Close();
                            warning_Error();
                        }
                        else
                        {
                            if (string.Equals(driverData[1], entryCity)) // handle if entry node is equal to exit node
                            {
                                fileDrivers.Close();
                                warning_Error();
                            }
                            else
                            {
                                DateTime currentTime = DateTime.Now;
                                DateTime enterTime = DateTime.Parse(driverData[3]);

                                if (string.Equals(driverData[4], "PM"))
                                    enterTime = enterTime.AddHours(12);

                                TimeSpan timeSpent = currentTime.Subtract(enterTime);
                                int index = Array.BinarySearch(cities, entryCity), index2 = Array.BinarySearch(cities, driverData[1]);
                                double distance = adjMatrix[index][index2];
                                TimeSpan minTimeSpent = TimeSpan.FromHours(distance / maxSpeed);

                                if (timeSpent < minTimeSpent)
                                    penalty = true;

                                StreamWriter writer;
                                double bill = coefficient[dropCategory.SelectedIndex] * (distance / 10.0); // every 100 miles add extra 10$
                                double paid;
                                double.TryParse(txtPaid.Text, out paid);

                                if (string.Equals(driverData[5], "0") && bill > paid)
                                {
                                    fileDrivers.Close();
                                    warning_Error();
                                }
                                else
                                {
                                    string breakLine = "==================================================";

                                    if (!File.Exists("Tickets.txt")) // if file doesn't exist, create a new one
                                    {
                                        using (writer = new StreamWriter("Tickets.txt"))
                                        {
                                            write_a_ticket(writer, ref breakLine, ref line, ref currentTime, index, index2, ref bill, ref driverData[5], ref paid);
                                        }
                                    }
                                    else // if file does exist, append in a existing one
                                    {
                                        using (writer = File.AppendText("Tickets.txt"))
                                        {
                                            write_a_ticket(writer, ref breakLine, ref line, ref currentTime, index, index2, ref bill, ref driverData[5], ref paid);
                                        }
                                    }

                                    fileDrivers.Close();

                                    txtRegPlate.Text = "";
                                    txtPaid.Text = "";
                                    dropCategory.Focus();
                                    labelSecSubmit.Visible = true;
                                    // create a timer and hide labelSecSubmit after 1.5 sec. using that timer
                                    var t = new Timer();
                                    t.Interval = 1500;
                                    t.Tick += (s, e2) =>
                                    {
                                        labelSecSubmit.Visible = false;
                                        t.Stop();
                                    };
                                    t.Start();

                                    lineChanger(lineCount); // delete processed driver from the database (text file)
                                }
                            }
                        }
                    }
                }
                else
                    warning_Error();
            }
        }

        // btnMinimize_MouseEnter && btnMinimize_MouseLeave simulate a button like behaviour on a picture box "btnClose"
        private void btnMinimize_MouseEnter(object sender, EventArgs e) => this.btnMinimize.BackColor = Color.FromArgb(14, 184, 167);

        private void btnMinimize_MouseLeave(object sender, EventArgs e) => this.btnMinimize.BackColor = Color.FromArgb(14, 154, 167);

        // show a tooltip when you hover over btnMinimize with a mouse
        private void btnMinimize_MouseHover(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.btnMinimize, "Minimize");
        }

        private void btnMinimize_Click(object sender, EventArgs e) => this.WindowState = FormWindowState.Minimized;

        // btnSubmit_MouseEnter && btnSubmit_MouseLeave && labelSubmit_MouseEnter && labelSubmit_MouseLeave
        // simulate a button like behaviour on a picture box "btnSubmit"
        private void btnSubmit_MouseEnter(object sender, EventArgs e) => this.btnSubmit.BackColor = Color.FromArgb(14, 184, 167);

        private void btnSubmit_MouseLeave(object sender, EventArgs e) => this.btnSubmit.BackColor = Color.FromArgb(14, 154, 167);

        private void txtRegPlate_TextChanged(object sender, EventArgs e)
        {
            if (txtRegPlate.Text == "")
            {
                labelPaid.Text = "Paid";
                labelRegPlate.Visible = true;
            }
            else // change live labelPaid to amount of $ it cost to exit a highway, if plate doesn't exist take no action
            {

                if (File.Exists("Drivers.txt"))
                {
                    string line;
                    StreamReader fileDrivers = new StreamReader("Drivers.txt");

                    while ((line = fileDrivers.ReadLine()) != null)
                    {
                        string[] sline = line.Split(' ');

                        if (string.Equals(sline[0], txtRegPlate.Text))
                            break;
                    }

                    if (line != null && dropCategory.Text != "")
                    {
                        string[] data = line.Split(' ');
                        double distance = adjMatrix[Array.BinarySearch(cities, entryCity)][Array.BinarySearch(cities, data[1])];
                        double bill = coefficient[dropCategory.SelectedIndex] * (distance / 10.0);
                        labelPaid.Text = bill.ToString() + "$";
                    }
                    else
                        labelPaid.Text = "Paid";

                    fileDrivers.Close();
                }
            }
        }

        private void labelRegPlate_Click(object sender, EventArgs e) => txtRegPlate.Focus();

        private void labelSubmit_MouseEnter(object sender, EventArgs e) => btnSubmit_MouseEnter(sender, e);

        private void labelSubmit_MouseLeave(object sender, EventArgs e) => btnSubmit_MouseLeave(sender, e);

        private void labelSubmit_Click(object sender, EventArgs e) => btnSubmit_Click(sender, e);

        private void txtRegPlate_KeyDown(object sender, KeyEventArgs e)
        {
            labelRegPlate.Visible = false;

            if (txtRegPlate.Text == "" && (e.KeyData == Keys.Back || e.KeyData == Keys.Delete))
            {
                labelRegPlate.Visible = true;
                labelPaid.Text = "Paid";
            }

            if (e.KeyData == Keys.Enter)
                btnSubmit_Click(sender, e);
        }

        private void submitToolStripMenuItem_Click(object sender, EventArgs e) => btnSubmit_Click(sender, e);

        // logout event
        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            LogIn li = new LogIn();
            li.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Do you really want to close the program?", "Warning", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
                Application.ExitThread();
        }

        private void txtPaid_KeyDown(object sender, KeyEventArgs e)
        {
            labelPaid.Visible = false;

            if (txtPaid.Text == "" && (e.KeyData == Keys.Back || e.KeyData == Keys.Delete))
                labelPaid.Visible = true;

            if (e.KeyData == Keys.Enter)
                btnSubmit_Click(sender, e);
        }

        private void txtPaid_TextChanged(object sender, EventArgs e)
        {
            if (txtPaid.Text == "") labelPaid.Visible = true;
        }

        private void labelPaid_Click(object sender, EventArgs e) => txtPaid.Focus();

        private void dropCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                btnSubmit_Click(sender, e);
        }

        private void dropCategory_SelectedIndexChanged(object sender, EventArgs e) => txtRegPlate_TextChanged(sender, e);

        // open About tab
        private void aboutHighwayControlToolStripMenuItem_Click(object sender, EventArgs e) => groupBoxAbout.Visible = true;

        // close About tab
        private void btnCloseGroup_Click(object sender, EventArgs e)
        {
            groupBoxAbout.Visible = false;
            labelRegPlate.Visible = labelPaid.Visible = true;
        }

        // simulate a button like behaviour on a picture box "btnCloseGroup"
        private void btnCloseGroup_MouseEnter(object sender, EventArgs e) => this.btnCloseGroup.BackColor = Color.Gainsboro;

        private void btnCloseGroup_MouseLeave(object sender, EventArgs e) => this.btnCloseGroup.BackColor = Color.Silver;
    }
}