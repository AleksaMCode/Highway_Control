﻿namespace HC_exit
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.submitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendFeedbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportAProblemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.provideASuggestionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutHighwayControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMinimize = new System.Windows.Forms.PictureBox();
            this.labelSubmit = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Panel();
            this.dropCategory = new System.Windows.Forms.ComboBox();
            this.txtRegPlate = new System.Windows.Forms.TextBox();
            this.labelRegPlate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelSecSubmit = new System.Windows.Forms.Label();
            this.labelPaid = new System.Windows.Forms.Label();
            this.txtPaid = new System.Windows.Forms.TextBox();
            this.groupBoxAbout = new System.Windows.Forms.GroupBox();
            this.richTextBoxAbout = new System.Windows.Forms.RichTextBox();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.btnCloseGroup = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimize)).BeginInit();
            this.btnSubmit.SuspendLayout();
            this.groupBoxAbout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCloseGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(154)))), ((int)(((byte)(167)))));
            this.menuStrip1.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(466, 27);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.submitToolStripMenuItem,
            this.toolStripSeparator1,
            this.logoutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 23);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // submitToolStripMenuItem
            // 
            this.submitToolStripMenuItem.Name = "submitToolStripMenuItem";
            this.submitToolStripMenuItem.Size = new System.Drawing.Size(131, 26);
            this.submitToolStripMenuItem.Text = "Submit";
            this.submitToolStripMenuItem.Click += new System.EventHandler(this.submitToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(128, 6);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(131, 26);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::HC_exit.Properties.Resources.Cancel_96;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(131, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewHelpToolStripMenuItem,
            this.sendFeedbackToolStripMenuItem,
            this.toolStripSeparator2,
            this.aboutHighwayControlToolStripMenuItem});
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 23);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // viewHelpToolStripMenuItem
            // 
            this.viewHelpToolStripMenuItem.Image = global::HC_exit.Properties.Resources.Question_Mark_96;
            this.viewHelpToolStripMenuItem.Name = "viewHelpToolStripMenuItem";
            this.viewHelpToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F11)));
            this.viewHelpToolStripMenuItem.Size = new System.Drawing.Size(292, 26);
            this.viewHelpToolStripMenuItem.Text = "View Help";
            this.viewHelpToolStripMenuItem.ToolTipText = "Coming Soon";
            // 
            // sendFeedbackToolStripMenuItem
            // 
            this.sendFeedbackToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportAProblemToolStripMenuItem,
            this.provideASuggestionToolStripMenuItem});
            this.sendFeedbackToolStripMenuItem.Name = "sendFeedbackToolStripMenuItem";
            this.sendFeedbackToolStripMenuItem.Size = new System.Drawing.Size(292, 26);
            this.sendFeedbackToolStripMenuItem.Text = "Send Feedback";
            this.sendFeedbackToolStripMenuItem.ToolTipText = "Coming Soon";
            // 
            // reportAProblemToolStripMenuItem
            // 
            this.reportAProblemToolStripMenuItem.Name = "reportAProblemToolStripMenuItem";
            this.reportAProblemToolStripMenuItem.Size = new System.Drawing.Size(233, 26);
            this.reportAProblemToolStripMenuItem.Text = "Report a Problem...";
            this.reportAProblemToolStripMenuItem.ToolTipText = "Coming Soon";
            // 
            // provideASuggestionToolStripMenuItem
            // 
            this.provideASuggestionToolStripMenuItem.Name = "provideASuggestionToolStripMenuItem";
            this.provideASuggestionToolStripMenuItem.Size = new System.Drawing.Size(233, 26);
            this.provideASuggestionToolStripMenuItem.Text = "Provide a Suggestion...";
            this.provideASuggestionToolStripMenuItem.ToolTipText = "Coming Soon";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(289, 6);
            // 
            // aboutHighwayControlToolStripMenuItem
            // 
            this.aboutHighwayControlToolStripMenuItem.Name = "aboutHighwayControlToolStripMenuItem";
            this.aboutHighwayControlToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.aboutHighwayControlToolStripMenuItem.Size = new System.Drawing.Size(292, 26);
            this.aboutHighwayControlToolStripMenuItem.Text = "About Highway Control";
            this.aboutHighwayControlToolStripMenuItem.Click += new System.EventHandler(this.aboutHighwayControlToolStripMenuItem_Click);
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(154)))), ((int)(((byte)(167)))));
            this.btnMinimize.Image = global::HC_exit.Properties.Resources.Compress_96_white;
            this.btnMinimize.Location = new System.Drawing.Point(436, -4);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(30, 30);
            this.btnMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnMinimize.TabIndex = 1;
            this.btnMinimize.TabStop = false;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            this.btnMinimize.MouseEnter += new System.EventHandler(this.btnMinimize_MouseEnter);
            this.btnMinimize.MouseLeave += new System.EventHandler(this.btnMinimize_MouseLeave);
            this.btnMinimize.MouseHover += new System.EventHandler(this.btnMinimize_MouseHover);
            // 
            // labelSubmit
            // 
            this.labelSubmit.AutoSize = true;
            this.labelSubmit.BackColor = System.Drawing.Color.Transparent;
            this.labelSubmit.Font = new System.Drawing.Font("Microsoft Tai Le", 13F, System.Drawing.FontStyle.Bold);
            this.labelSubmit.ForeColor = System.Drawing.Color.White;
            this.labelSubmit.Location = new System.Drawing.Point(15, 6);
            this.labelSubmit.Name = "labelSubmit";
            this.labelSubmit.Size = new System.Drawing.Size(87, 29);
            this.labelSubmit.TabIndex = 7;
            this.labelSubmit.Text = "Submit";
            this.labelSubmit.Click += new System.EventHandler(this.labelSubmit_Click);
            this.labelSubmit.MouseEnter += new System.EventHandler(this.labelSubmit_MouseEnter);
            this.labelSubmit.MouseLeave += new System.EventHandler(this.labelSubmit_MouseLeave);
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(154)))), ((int)(((byte)(167)))));
            this.btnSubmit.Controls.Add(this.labelSubmit);
            this.btnSubmit.Location = new System.Drawing.Point(175, 136);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(117, 40);
            this.btnSubmit.TabIndex = 9;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            this.btnSubmit.MouseEnter += new System.EventHandler(this.btnSubmit_MouseEnter);
            this.btnSubmit.MouseLeave += new System.EventHandler(this.btnSubmit_MouseLeave);
            // 
            // dropCategory
            // 
            this.dropCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropCategory.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.dropCategory.FormattingEnabled = true;
            this.dropCategory.Items.AddRange(new object[] {
            "A - Motor",
            "B - Car",
            "C - Truck",
            "D - Bus"});
            this.dropCategory.Location = new System.Drawing.Point(25, 76);
            this.dropCategory.MaxDropDownItems = 4;
            this.dropCategory.Name = "dropCategory";
            this.dropCategory.Size = new System.Drawing.Size(121, 33);
            this.dropCategory.TabIndex = 10;
            this.dropCategory.SelectedIndexChanged += new System.EventHandler(this.dropCategory_SelectedIndexChanged);
            this.dropCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dropCategory_KeyDown);
            // 
            // txtRegPlate
            // 
            this.txtRegPlate.BackColor = System.Drawing.Color.White;
            this.txtRegPlate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRegPlate.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.txtRegPlate.Location = new System.Drawing.Point(165, 76);
            this.txtRegPlate.Name = "txtRegPlate";
            this.txtRegPlate.Size = new System.Drawing.Size(183, 33);
            this.txtRegPlate.TabIndex = 11;
            this.txtRegPlate.TextChanged += new System.EventHandler(this.txtRegPlate_TextChanged);
            this.txtRegPlate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRegPlate_KeyDown);
            // 
            // labelRegPlate
            // 
            this.labelRegPlate.AutoSize = true;
            this.labelRegPlate.BackColor = System.Drawing.Color.White;
            this.labelRegPlate.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelRegPlate.Font = new System.Drawing.Font("Microsoft Tai Le", 11F);
            this.labelRegPlate.ForeColor = System.Drawing.Color.Silver;
            this.labelRegPlate.Location = new System.Drawing.Point(172, 81);
            this.labelRegPlate.Name = "labelRegPlate";
            this.labelRegPlate.Size = new System.Drawing.Size(158, 23);
            this.labelRegPlate.TabIndex = 12;
            this.labelRegPlate.Text = "Registration plate";
            this.labelRegPlate.Click += new System.EventHandler(this.labelRegPlate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(165, 204);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 13;
            // 
            // labelSecSubmit
            // 
            this.labelSecSubmit.AutoSize = true;
            this.labelSecSubmit.Font = new System.Drawing.Font("Microsoft Tai Le", 10F);
            this.labelSecSubmit.Location = new System.Drawing.Point(145, 182);
            this.labelSecSubmit.Name = "labelSecSubmit";
            this.labelSecSubmit.Size = new System.Drawing.Size(176, 22);
            this.labelSecSubmit.TabIndex = 14;
            this.labelSecSubmit.Text = "Successful submission";
            this.labelSecSubmit.Visible = false;
            // 
            // labelPaid
            // 
            this.labelPaid.AutoSize = true;
            this.labelPaid.BackColor = System.Drawing.Color.White;
            this.labelPaid.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelPaid.Font = new System.Drawing.Font("Microsoft Tai Le", 11F);
            this.labelPaid.ForeColor = System.Drawing.Color.Silver;
            this.labelPaid.Location = new System.Drawing.Point(362, 81);
            this.labelPaid.Name = "labelPaid";
            this.labelPaid.Size = new System.Drawing.Size(47, 23);
            this.labelPaid.TabIndex = 16;
            this.labelPaid.Text = "Paid";
            this.labelPaid.Click += new System.EventHandler(this.labelPaid_Click);
            // 
            // txtPaid
            // 
            this.txtPaid.BackColor = System.Drawing.Color.White;
            this.txtPaid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPaid.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.txtPaid.Location = new System.Drawing.Point(354, 76);
            this.txtPaid.Name = "txtPaid";
            this.txtPaid.Size = new System.Drawing.Size(88, 33);
            this.txtPaid.TabIndex = 15;
            this.txtPaid.TextChanged += new System.EventHandler(this.txtPaid_TextChanged);
            this.txtPaid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPaid_KeyDown);
            // 
            // groupBoxAbout
            // 
            this.groupBoxAbout.Controls.Add(this.richTextBoxAbout);
            this.groupBoxAbout.Controls.Add(this.pictureBoxLogo);
            this.groupBoxAbout.Controls.Add(this.btnCloseGroup);
            this.groupBoxAbout.Font = new System.Drawing.Font("Microsoft Tai Le", 7.8F);
            this.groupBoxAbout.Location = new System.Drawing.Point(0, 43);
            this.groupBoxAbout.Name = "groupBoxAbout";
            this.groupBoxAbout.Size = new System.Drawing.Size(466, 223);
            this.groupBoxAbout.TabIndex = 17;
            this.groupBoxAbout.TabStop = false;
            this.groupBoxAbout.Text = "About Highway Control";
            this.groupBoxAbout.Visible = false;
            // 
            // richTextBoxAbout
            // 
            this.richTextBoxAbout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(154)))), ((int)(((byte)(167)))));
            this.richTextBoxAbout.Font = new System.Drawing.Font("Microsoft Tai Le", 8.5F);
            this.richTextBoxAbout.ForeColor = System.Drawing.Color.White;
            this.richTextBoxAbout.Location = new System.Drawing.Point(95, 63);
            this.richTextBoxAbout.Name = "richTextBoxAbout";
            this.richTextBoxAbout.ReadOnly = true;
            this.richTextBoxAbout.Size = new System.Drawing.Size(371, 97);
            this.richTextBoxAbout.TabIndex = 0;
            this.richTextBoxAbout.Text = resources.GetString("richTextBoxAbout.Text");
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Image = global::HC_exit.Properties.Resources.logo_road_blue;
            this.pictureBoxLogo.Location = new System.Drawing.Point(-17, 60);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(127, 108);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TabIndex = 7;
            this.pictureBoxLogo.TabStop = false;
            // 
            // btnCloseGroup
            // 
            this.btnCloseGroup.Image = global::HC_exit.Properties.Resources.Cancel_96;
            this.btnCloseGroup.Location = new System.Drawing.Point(447, 9);
            this.btnCloseGroup.Name = "btnCloseGroup";
            this.btnCloseGroup.Size = new System.Drawing.Size(18, 18);
            this.btnCloseGroup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnCloseGroup.TabIndex = 7;
            this.btnCloseGroup.TabStop = false;
            this.btnCloseGroup.Click += new System.EventHandler(this.btnCloseGroup_Click);
            this.btnCloseGroup.MouseEnter += new System.EventHandler(this.btnCloseGroup_MouseEnter);
            this.btnCloseGroup.MouseLeave += new System.EventHandler(this.btnCloseGroup_MouseLeave);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(466, 264);
            this.Controls.Add(this.groupBoxAbout);
            this.Controls.Add(this.labelPaid);
            this.Controls.Add(this.txtPaid);
            this.Controls.Add(this.labelRegPlate);
            this.Controls.Add(this.labelSecSubmit);
            this.Controls.Add(this.dropCategory);
            this.Controls.Add(this.txtRegPlate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimize)).EndInit();
            this.btnSubmit.ResumeLayout(false);
            this.btnSubmit.PerformLayout();
            this.groupBoxAbout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCloseGroup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.PictureBox btnMinimize;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem submitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendFeedbackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportAProblemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem provideASuggestionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem aboutHighwayControlToolStripMenuItem;
        private System.Windows.Forms.Label labelSubmit;
        private System.Windows.Forms.Panel btnSubmit;
        private System.Windows.Forms.ComboBox dropCategory;
        private System.Windows.Forms.TextBox txtRegPlate;
        private System.Windows.Forms.Label labelRegPlate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelSecSubmit;
        private System.Windows.Forms.Label labelPaid;
        private System.Windows.Forms.TextBox txtPaid;
        private System.Windows.Forms.GroupBox groupBoxAbout;
        private System.Windows.Forms.RichTextBox richTextBoxAbout;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.PictureBox btnCloseGroup;
    }
}