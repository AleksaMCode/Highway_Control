﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; // use System.IO namespace for reading and writing to files and data streams

namespace HC_exit
{
    public partial class LogIn : Form
    {
        string[] array;
        public LogIn()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text == "" || txtPassword.Text == "") // handle error input
            {
                MessageBox.Show("Invalid Login. Please enter correct username or password.", "Warning", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                labelUsername.Visible = true;
                txtUsername.Text = "";
                labelPassword.Visible = true;
                txtPassword.Text = "";
                txtUsername.Focus();
            }
            else // read line from a text file and compare user entered username and password with ones already in a database (text file)
            {
                string line;
                StreamReader file = new StreamReader("Users_exit.txt");
                while ((line = file.ReadLine()) != null)
                {
                    array = line.Split(' ');
                    string username = array[0],
                        password = array[1];
                    if (string.Equals(username, txtUsername.Text)
                        && string.Equals(password, txtPassword.Text))
                        break;
                }
                file.Close();
                if (line == null) // handle wrong username or password input (no match is found)
                {
                    MessageBox.Show("Wrong username or password.");
                    labelUsername.Visible = true;
                    txtUsername.Text = "";
                    labelPassword.Visible = true;
                    txtPassword.Text = "";
                    txtUsername.Focus();
                }
                else
                {
                    // close this wform and open MainForm
                    this.Close();
                    MainForm mainForm = new MainForm();
                    mainForm.entryCity = array[2]; // transfer node (entry location) to Main form
                    mainForm.operatorsName = array[0]; // transfer users name to Main form
                    mainForm.Show();
                }
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            labelPassword.Visible = false;

            if (txtPassword.Text == "" && (e.KeyData == Keys.Back || e.KeyData == Keys.Delete))
                labelPassword.Visible = true;

            if (e.KeyData == Keys.Enter)
                btnLogin_Click(sender, e);
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            labelUsername.Visible = false;

            if (txtUsername.Text == "" && (e.KeyData == Keys.Back || e.KeyData == Keys.Delete))
                labelUsername.Visible = true;

            if (e.KeyData == Keys.Enter)
                btnLogin_Click(sender, e);
        }

        // btnClose_MouseEnter && btnClose_MouseLeave simulate a button like behaviour on a picture box "btnClose"
        private void btnClose_MouseEnter(object sender, EventArgs e) => this.btnClose.BackColor = Color.FromArgb(14, 184, 167);

        private void btnClose_MouseLeave(object sender, EventArgs e) => this.btnClose.BackColor = Color.FromArgb(14, 154, 167);

        // btnLogin_MouseEnter && btnLogin_MouseLeave && labelLogin_MouseEnter && labelLogin_MouseLeave
        // simulate a button like behaviour on a picture box "btnLogin"
        private void btnLogin_MouseEnter(object sender, EventArgs e) => this.btnLogin.BackColor = Color.FromArgb(14, 184, 167);

        private void btnLogin_MouseLeave(object sender, EventArgs e) => this.btnLogin.BackColor = Color.FromArgb(14, 154, 167);

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {
            if (txtUsername.Text == "") labelUsername.Visible = true;
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            if (txtPassword.Text == "") labelPassword.Visible = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Do you really want to close the program?", "Warning", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
                Application.ExitThread();
        }

        private void labelLogin_MouseEnter(object sender, EventArgs e) => btnLogin_MouseEnter(sender, e);

        private void labelLogin_MouseLeave(object sender, EventArgs e) => btnLogin_MouseLeave(sender, e);

        private void labelLogin_Click(object sender, EventArgs e) => btnLogin_Click(sender, e);

        private void labelUsername_Click(object sender, EventArgs e) => txtUsername.Focus();

        private void labelPassword_Click(object sender, EventArgs e) => txtPassword.Focus();

        // show a tooltip when you hover over btnClose with a mouse
        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.btnClose, "Exit");
        }
    }
}
