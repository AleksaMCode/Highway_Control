﻿    using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HC_exit
{
    public partial class SplashScreen : Form
    {
        int count = 0, buffer = 0;
        public SplashScreen()
        {
            // create transparent back color for this wform
            this.BackColor = Color.LimeGreen;
            this.TransparencyKey = Color.LimeGreen;

            InitializeComponent();
            timer1.Tick += new EventHandler(timer1_Tick_1);
            timer2.Tick += new EventHandler(timer2_Tick);
            timer3.Tick += new EventHandler(timer3_Tick);
            // set opacaity for this wfrom to 0
            Opacity = 0;
            // start first out of three timers
            timer1.Start();
        }

        protected override void OnPaintBackground(PaintEventArgs e) => e.Graphics.FillRectangle(Brushes.LimeGreen, e.ClipRectangle);

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (buffer == 3)
            {
                timer3.Start();
                timer2.Stop();
            }
            else
                ++buffer;
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            if (Opacity == 1)
            {
                timer2.Start();
                timer1.Stop();
            }
            else
            {
                ++count;
                Opacity = count * 0.01;
            }
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            if (Opacity == 0)
            {
                // when animation ends open LogIn form and hide SplashScreen form
                LogIn newform = new LogIn();
                newform.Show();
                Hide();
                timer3.Stop();
            }
            else
            {
                --count;
                Opacity = count * 0.01;
            }
        }
    }
}
